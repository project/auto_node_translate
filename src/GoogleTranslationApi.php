<?php

namespace Drupal\auto_node_translate;

use Google\Cloud\Translate\V3\TranslationServiceClient;
use Drupal\file\Entity\File;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Google\Cloud\Translate\V3\TranslateTextGlossaryConfig;

/**
 * Description of GoogleTranslationAPI.
 *
 * Implementation of TranslationApiInterface for Google Cloud Translation V3.
 */
class GoogleTranslationApi implements TranslationApiInterface {
  use StringTranslationTrait;

  /**
   * Google\Cloud\Translate\V3\TranslationServiceClient definition.
   *
   * @var Google\Cloud\Translate\V3\TranslationServiceClient
   */
  private $translationServiceClient;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $config = \Drupal::config('auto_node_translate.config');
    $credentials = $config->get('google_credentials');
    if (!empty($credentials[0])) {
      $credentialsFile = File::load($credentials[0]);
    }
    $absolute_path = (!empty($credentialsFile)) ? \Drupal::service('file_system')->realpath($credentialsFile->getFileUri()) : NULL;
    $options = [
      'credentials' => $absolute_path,
    ];
    try {
      $this->translationServiceClient = new TranslationServiceClient($options);
    }
    catch (\Exception $th) {
      \Drupal::messenger()->addError($this->t('Error @code: @message', [
        '@code' => $th->getCode(),
        '@message' => $th->getMessage(),
      ]));
      $this->translationServiceClient = NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function translate($text, $languageFrom, $languageTo, $customTranslations = []) {
    if (is_null($this->translationServiceClient)) {
      return $text;
    }
    $text = $this->removeUntranslatable($text, $customTranslations);
    $text = $this->removeCustomTranslations($text, $customTranslations, $languageTo);
    $config = \Drupal::config('auto_node_translate.config');
    $projectId = $config->get('google_api_project');
    $location = $config->get('google_location');
    if (strlen($text) > 20000) {
      return $this->translate(substr($text, 0, 20000), $languageFrom, $languageTo, $customTranslations) . $this->translate(substr($text, 20000), $languageFrom, $languageTo, $customTranslations);
    }
    else {
      try {
        $contents = [$text];
        $targetLanguageCode = $languageTo;
        $formattedParent = $this->translationServiceClient->locationName($projectId, $location);
        $options = [
          'sourceLanguageCode' => $languageFrom,
        ];
        $glossary = $config->get('google_api_glossary_mappings_' . $languageTo);
        if (!empty($glossary)) {
          $glossaryConfig = new TranslateTextGlossaryConfig(
            [
              'glossary' => 'projects/' . $projectId . '/locations/' . $location . '/glossaries/' . $glossary,
              'ignore_case' => TRUE,
            ],
          );
          $options += [
            'glossaryConfig' => $glossaryConfig,
          ];
        }
        $response = $this->translationServiceClient->translateText($contents, $targetLanguageCode, $formattedParent, $options);
        $translation = $response->getTranslations()->offsetGet(0)->getTranslatedText();

        $translation = $this->addUntranslatable($translation, $customTranslations);
        $translation = $this->addCustomTranslations($translation, $customTranslations, $languageTo);
      }
      catch (\Exception $th) {
        \Drupal::messenger()->addError($this->t('Error @code: @message', [
          '@code' => $th->getCode(),
          '@message' => $th->getMessage(),
        ]));
      }
      finally {
        $this->translationServiceClient->close();
      }
    }
    \Drupal::messenger()->addStatus($this->t('Translation Completed'));

    return (empty($translation)) ? $text : $translation;
  }

  /**
   * Removes Untranslatable.
   *
   * @param mixed $text
   *   The text.
   * @param mixed $customTranslations
   *   The custom translation.
   *
   * @return mixed
   *   The returned text.
   */
  public function removeUntranslatable($text, $customTranslations) {
    if (isset($customTranslations['untrans'])) {
      foreach ($customTranslations['untrans'] as $key => $value) {
        $text = str_replace($key, $value, $text);
      }
    }
    return $text;
  }

  /**
   * Adds Untranslatable.
   *
   * @param mixed $translatedText
   *   The translated text.
   * @param mixed $customTranslations
   *   The custom translation.
   *
   * @return mixed
   *   The returned translated text.
   */
  public function addUntranslatable($translatedText, $customTranslations) {
    if (!empty($customTranslations['untrans'])) {
      foreach ($customTranslations['untrans'] as $key => $value) {
        $translatedText = str_replace($value, $key, $translatedText);
      }
    }
    return $translatedText;
  }

  /**
   * Removes Custom Translations.
   *
   * @param mixed $text
   *   The text.
   * @param mixed $customTranslations
   *   The custom translation.
   * @param mixed $languageTo
   *   The language to translate to.
   *
   * @return mixed
   *   The returned translated text.
   */
  public function removeCustomTranslations($text, $customTranslations, $languageTo) {
    if (isset($customTranslations[$languageTo])) {
      foreach ($customTranslations[$languageTo] as $key => $value) {
        $text = str_replace($key, $value['untrans'], $text);
      }
    }
    return $text;
  }

  /**
   * Adds Custom Translations.
   *
   * @param mixed $translatedText
   *   The text.
   * @param mixed $customTranslations
   *   The custom translation.
   * @param mixed $languageTo
   *   The language to translate to.
   *
   * @return mixed
   *   The returned translated text.
   */
  public function addCustomTranslations($translatedText, $customTranslations, $languageTo) {
    if (isset($customTranslations[$languageTo])) {
      foreach ($customTranslations[$languageTo] as $value) {
        $translatedText = str_replace($value['untrans'], $value['to'], $translatedText);
      }
    }
    return $translatedText;
  }

}
