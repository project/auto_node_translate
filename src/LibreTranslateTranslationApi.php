<?php

namespace Drupal\auto_node_translate;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Description of LibreTranslateTranslationApi.
 *
 * Implementation of TranslationApiInterface for LibreTranslate.
 */
class LibreTranslateTranslationApi implements TranslationApiInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function translate($text, $languageFrom, $languageTo, $customTranslations = []) {
    // Elements to untranslate.
    $text = $this->removeUntranslatable($text, $customTranslations);
    // Custom translation.
    $text = $this->removeCustomTranslations($text, $customTranslations, $languageTo);
    $config = \Drupal::config('auto_node_translate.config');
    $from = explode('-', $languageFrom)[0];
    $to = explode('-', $languageTo)[0];
    $apikey = $config->get('libretranslate_apikey');
    $url = rtrim($config->get('libretranslate_url'), '/');

    if ($this->modelIsAvailable($from, $to, $url)) {
      $client = \Drupal::httpClient();
      try {
        $response = $client->post($url . '/translate', [
          'form_params' => [
            'api_key' => $apikey,
            'q' => $text,
            'source' => $from,
            'target' => $to,
            'format' => preg_match('#(?<=<)\w+(?=[^<]*?>)#', $text) ? 'html' : 'text',
          ],
        ]);
      }
      catch (\Exception $e) {
        $link = Url::fromRoute('auto_node_translate.config_form', [], ['absolute' => TRUE])->toString();
        \Drupal::messenger()->addError($this->t('LibreTranslate translator error @error .Try another Api in <a href="@link">@link</a>', [
          '@link' => $link,
          '@error' => $e->getMessage(),
        ]));
        return $text;
      }
      $formatedData = Json::decode($response->getBody()->getContents());
      if (isset($formatedData['translatedText'])) {
        $translatedText = $formatedData['translatedText'];
      }
      else {
        $translatedText = $text;
        \Drupal::messenger()->addStatus($this->t('The translation failed for @text', ['@text' => $text]));
      }
    }
    else {
      $link = Url::fromRoute('auto_node_translate.config_form', [], ['absolute' => TRUE])->toString();
      $placeholders = [
        '@link' => $link,
        '@from' => $from,
        '@to' => $to,
      ];
      \Drupal::messenger()->addStatus($this->t("The model @from-@to isn't available in LibreTranslate. Try a different api in <a href=@link>@link</a> or try to translate form english as original language", $placeholders));
      $translatedText = $text;
    }
    \Drupal::messenger()->addStatus($this->t('Translation Completed'));
    $translatedText = $this->addUntranslatable($translatedText, $customTranslations);
    $translatedText = $this->addCustomTranslations($translatedText, $customTranslations, $languageTo);
    return $translatedText;
  }

  /**
   * {@inheritdoc}
   */
  public function modelIsAvailable($languageFrom, $languageTo, $url) {
    $client = \Drupal::httpClient();
    try {
      $response = $client->get($url . '/languages', []);
      $languages = Json::decode($response->getBody()->getContents());
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($this->t('LibreTranslateTranslationApi error @error .', [
        '@error' => $e->getMessage(),
      ]));
      return FALSE;
    }

    foreach ($languages as $value) {
      if ($value['code'] == $languageFrom) {
        if (in_array($languageTo, $value['targets'])) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Removes Untranslatable.
   *
   * @param mixed $text
   *   The text.
   * @param mixed $customTranslations
   *   The custom translation.
   *
   * @return mixed
   *   The returned text.
   */
  public function removeUntranslatable($text, $customTranslations) {
    if (isset($customTranslations['untrans'])) {
      foreach ($customTranslations['untrans'] as $key => $value) {
        $text = str_replace($key, $value, $text);
      }
    }
    return $text;
  }

  /**
   * Adds Untranslatable.
   *
   * @param mixed $translatedText
   *   The translated text.
   * @param mixed $customTranslations
   *   The custom translation.
   *
   * @return mixed
   *   The returned translated text.
   */
  public function addUntranslatable($translatedText, $customTranslations) {
    if (!empty($customTranslations['untrans'])) {
      foreach ($customTranslations['untrans'] as $key => $value) {
        $translatedText = str_replace($value, $key, $translatedText);
      }
    }

    return $translatedText;
  }

  /**
   * Removes Custom Translations.
   *
   * @param mixed $text
   *   The text.
   * @param mixed $customTranslations
   *   The custom translation.
   * @param mixed $languageTo
   *   The language to translate to.
   *
   * @return mixed
   *   The returned translated text.
   */
  public function removeCustomTranslations($text, $customTranslations, $languageTo) {
    if (isset($customTranslations[$languageTo])) {
      foreach ($customTranslations[$languageTo] as $key => $value) {
        $text = str_replace($key, $value['untrans'], $text);
      }
    }
    return $text;
  }

  /**
   * Adds Custom Translations.
   *
   * @param mixed $translatedText
   *   The text.
   * @param mixed $customTranslations
   *   The custom translation.
   * @param mixed $languageTo
   *   The language to translate to.
   *
   * @return mixed
   *   The returned translated text.
   */
  public function addCustomTranslations($translatedText, $customTranslations, $languageTo) {
    if (isset($customTranslations[$languageTo])) {
      foreach ($customTranslations[$languageTo] as $value) {
        $translatedText = str_replace($value['untrans'], $value['to'], $translatedText);
      }
    }
    return $translatedText;
  }

}
